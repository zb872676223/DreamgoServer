#ifndef NET_POLLER_H
#define NET_POLLER_H

#include <map>
#include <vector>

#include "../base/timestamp.h"
#include "EventLoop.h"

class Channel;
//PollFdList和ChannelMap中保存关心的文件描述符和对应的Chnnel

class Poller
{
public:
	typedef std::vector<Channel*> ChannelList;

	Poller(EventLoop* loop);
	virtual ~Poller();

	/// Polls the I/O events.
	/// Must be called in the loop thread.
	virtual Timestamp poll(int timeoutMs, ChannelList* activeChannels) = 0;

	/// Changes the interested I/O events.
	/// Must be called in the loop thread.
	virtual void updateChannel(Channel* channel) = 0;

	/// Remove the channel, when it destructs.
	/// Must be called in the loop thread.
	virtual void removeChannel(Channel* channel) = 0;

	bool hasChannel(Channel* channel);

	static Poller* newDefaultPoller(EventLoop* loop);

	void assertInLoopThread()const { ownerLoop_->assertInLoopThread(); }

	Poller(const Poller&) = delete;
	const Poller& operator=(const Poller&) = delete;
protected:
	typedef std::map<int, Channel*> ChannelMap;
	ChannelMap channels_;
private:
	EventLoop* ownerLoop_;
};

#endif  // NET_POLLER_H
