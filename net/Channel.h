#ifndef NET_CHANNEL_H
#define NET_CHANNEL_H

#include <memory>
#include <functional>
class Timestamp;
class EventLoop;
//Channel 通过enableXXX类函数来注册fd上的新事件
///
/// A selectable I/O channel.
///
/// This class doesn't own the file descriptor.
/// The file descriptor could be a socket,
/// an eventfd, a timerfd, or a signalfd
class Channel
{
public:
	typedef std::function<void()> EventCallback;
	typedef std::function<void(Timestamp)> ReadEventCallback;
	Channel(EventLoop* loop, int fd);
	~Channel();

	void handleEvent(Timestamp receiveTime);
	void setReadCallback(const ReadEventCallback& cb)
	{ readCallback_ = cb; }
	void setWriteCallback(const EventCallback& cb)
	{ writeCallback_ = cb; }
	void setErrorCallback(const EventCallback& cb)
	{ errorCallback_ = cb; }
	void setCloseCallback(const EventCallback& cb)
	{ closeCallback_ = cb; }
	int fd() const { return fd_; }
	int events() const { return events_; }
	void set_revents(int revt) { revents_ = revt; }
	bool isNoneEvent() const { return events_ == kNoneEvent; }

	void enableReading() { events_ |= kReadEvent; update(); }
	void enableWriting() { events_ |= kWriteEvent; update(); }
	void disableWriting() { events_ &= ~kWriteEvent; update(); }
	void disableReading() { events_ &= ~kReadEvent; update(); }
	void disableAll() { events_ = kNoneEvent; update(); }
	bool isWriting()const{ return events_ & kWriteEvent; }
	bool isReading()const{ return events_ & kReadEvent; }
	// for Poller
	int index() { return index_; }
	void set_index(int idx) { index_ = idx; }
	
	// for debug
	std::string reventsToString() const;

	void remove();
	EventLoop* ownerLoop() { return loop_; }

	/// Tie this channel to the owner object managed by shared_ptr,
	/// prevent the owner object being destroyed in handleEvent.
	void tie(const std::shared_ptr<void>& obj);

	Channel(const Channel&) = delete;
	const Channel& operator=(const Channel&) = delete;
private:
	void update();
	void handleEventWithGuard(Timestamp receiveTime);

	static const int kNoneEvent;
	static const int kReadEvent;
	static const int kWriteEvent;

	EventLoop* loop_;
	const int  fd_;
	int        events_;
	int        revents_;
	int        index_; // used by Poller.Poller中保存一个channel数组
	bool       addedToLoop_;
	bool       eventHandling_;
	std::weak_ptr<void> tie_;
	bool tied_;

	ReadEventCallback readCallback_;
	EventCallback writeCallback_;
	EventCallback errorCallback_;
	EventCallback closeCallback_;
};
#endif  //NET_CHANNEL_H
