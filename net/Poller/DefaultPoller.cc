#include "../Poller.h"
#include "PollPoller.h"
#include "EPollPoller.h"
#include "../../base/logging.h"
#include <stdlib.h>

Poller* Poller::newDefaultPoller(EventLoop* loop)
{
	
	//if (::setenv("MUDUO_USE_POLL", "POLL", 1))
	//{
	//	LOG_WARN << "setenv error : " << strerror(errno);
	//}

	if (::getenv("MUDUO_USE_POLL"))
	{
		return new PollPoller(loop);
	}else
	{
		return new EPollPoller(loop);
	}
}