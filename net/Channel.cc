#include "Channel.h"
#include "EventLoop.h"
#include "../base/logging.h"

#include <poll.h>
#include <sstream>

const int Channel::kNoneEvent = 0;
const int Channel::kReadEvent = POLLIN | POLLPRI;
const int Channel::kWriteEvent = POLLOUT;

Channel::Channel(EventLoop* loop, int fdArg)
  : loop_(loop),
    fd_(fdArg),
    events_(0),
    revents_(0),
    index_(-1),
	addedToLoop_(false),
	eventHandling_(false),
	tied_(false)
{
	//LOG_DEBUG << "Channel ctor[" << this << "] fd = " << fd_;
}

Channel::~Channel()
{
	//LOG_DEBUG << "Channel dtor[" << this << "] fd = " << fd_;
	//防止出现Channel对象已经析构的情况下调用handleEvent()
	assert(!eventHandling_);
	assert(!addedToLoop_);
}

void Channel::update()
{
	addedToLoop_ = true;
	loop_->updateChannel(this);
}

void Channel::handleEventWithGuard(Timestamp receiveTime)
{
	eventHandling_ = true;
	LOG_TRACE << reventsToString();
	if ((revents_ & POLLHUP) && !(revents_ & POLLIN))
	{
		LOG_WARN << "Channel::handle_event() POLLHUP";
		if (closeCallback_) closeCallback_();
	}

	if (revents_ & POLLNVAL)//文件描述符没有打开
	{
		LOG_WARN << "Channel::handle_event() POLLNVAL";
	}

	if (revents_ & (POLLERR | POLLNVAL))
	{
		if (errorCallback_) errorCallback_();
	}
	//POLLRDHUP它在socket上接收到对方关闭连接的请求之后触发。使用此事件需要在代码最开始处定义_GNU_SOURCE
	if (revents_ & (POLLIN | POLLPRI | POLLRDHUP))
	{
		//当是侦听socket时，readCallback_指向Acceptor::handleRead
		//当是客户端socket时，调用TcpConnection::handleRead 
		if (readCallback_) readCallback_(receiveTime);
	}
	if (revents_ & POLLOUT)
	{
		//如果是连接状态服的socket，则writeCallback_指向Connector::handleWrite()
		if (writeCallback_) writeCallback_();
	}
	eventHandling_ = false;
}

std::string Channel::reventsToString() const
{
	std::ostringstream oss;
	oss << fd_ << ": ";
	if (revents_ & POLLIN)
		oss << "IN ";
	if (revents_ & POLLPRI)
		oss << "PRI ";
	if (revents_ & POLLOUT)
		oss << "OUT ";
	if (revents_ & POLLHUP)
		oss << "HUP ";
	if (revents_ & POLLRDHUP)
		oss << "RDHUP ";
	if (revents_ & POLLERR)
		oss << "ERR ";
	if (revents_ & POLLNVAL)
		oss << "NVAL ";

	return oss.str().c_str();
}

void Channel::remove()
{
	assert(isNoneEvent());
	addedToLoop_ = false;
	loop_->removeChannel(this);
}

void Channel::tie(const std::shared_ptr<void>& obj)
{
	tie_ = obj;
	tied_ = true;
}

void Channel::handleEvent(Timestamp receiveTime)
{
	std::shared_ptr<void> guard;
	if (tied_)
	{
		guard = tie_.lock();
		if (guard)
		{
			handleEventWithGuard(receiveTime);
		}
	}
	else
	{
		handleEventWithGuard(receiveTime);
	}
}
