#include "TcpSession.h"
#include "../base/logging.h"
#include "Msg.h"

TcpSession::TcpSession(const std::weak_ptr<TcpConnection>& tmpconn) : m_tmpConn(tmpconn)
{

}

TcpSession::~TcpSession()
{

}

//+------------+--------------------------------------------------------------------------------------+
//|    包头    |								数据								                  |
//+------------+------------------+--------------------+----------+----------+------------------------+
//|msg(struct) |   PACKLEN(int)   |CHECKSUM_LEN(short) | cmd(int) | seq(int) | data(datalen+data)     |
//+------------+------------------+--------------------+----------+----------+------------------------+
//发送数据的时候数据前面加上一个包体的大小
void TcpSession::Send(const std::string& buf)
{
	std::string strSendData;
	msg header = { static_cast<int32_t>(buf.length()) };
	LOG_INFO << "Send data, header length:" << sizeof(header) << ", body length:" << buf.length();
	strSendData.append(reinterpret_cast<const char*>(&header), sizeof(header));
	strSendData.append(buf.c_str(), buf.length());
	if (m_tmpConn.expired())
	{
		//FIXME: 出现这种问题需要排查
		LOG_ERROR << "Tcp connection is destroyed , but why TcpSession is still alive ?";
		return;
	}
	TcpConnectionPtr conn = m_tmpConn.lock();
	if (conn)
	{
		size_t length = strSendData.length();
		//LOG_INFO << "Send data, length:" << length;
		LOG_DEBUG_BIN((unsigned char*)strSendData.c_str(), length);
		conn->send(strSendData.c_str(), length);
	}
}

void TcpSession::Send(const char* p, int length)
{
	if (m_tmpConn.expired())
	{
		//FIXME: 出现这种问题需要排查
		LOG_ERROR << "Tcp connection is destroyed , but why TcpSession is still alive ?";
		return;
	}
	//TODO: 这些Session和connection对象的生命周期要好好梳理一下
	std::shared_ptr<TcpConnection> conn = m_tmpConn.lock();
	if (conn)
	{
		LOG_INFO << "Send data, length:" << length;
		LOG_DEBUG_BIN((unsigned char*)p, length);
		conn->send(p, length);
	}
}