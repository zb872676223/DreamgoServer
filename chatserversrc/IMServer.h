/******************************************************************
Copyright (C) 2018 - All Rights Reserved by
文 件 名 : IMServer.h --- IMServer
编写日期 : 2018/12/7 
说 明    : 服务器主服务类，对新连接的侦听和处理
历史纪录 :
<作者>    <日期>        <版本>        <内容>
*******************************************************************/
#pragma once
#include <memory>
#include <map>
#include <mutex>
#include "../net/TcpServer.h"
#include "../net/EventLoop.h"
#include "ClientSession.h"

typedef std::shared_ptr<ClientSession> ClientSessionPtr;
struct StoredUserInfo
{
	int32_t         userid;
	std::string     username;
	std::string     password;
	std::string     nickname;
};

class IMServer final
{
public:
	IMServer() = default;
	~IMServer() = default;

	IMServer(const IMServer& rhs) = delete;
	IMServer& operator =(const IMServer& rhs) = delete;

	bool Init(const char* ip, short port, EventLoop* loop);
	bool IsUserSessionExsit(int32_t userid);
	bool GetSessionByUserId(std::shared_ptr<ClientSession>& session, int32_t userid);
private:
	//新连接到来或连接断开时候调用，所以需要通过conn->connected()来判断，一般只在主loop（工作loop?）里面调用
	void OnConnection(TcpConnectionPtr conn);
	//连接断开
	void OnClose(const TcpConnectionPtr& conn);

private:
	std::shared_ptr<TcpServer>					m_server;
	std::map<std::string,ClientSessionPtr>		m_mapSessions;
	std::mutex									m_sessionMutex;		//多线程之间保护m_sessions
	int											m_baseUserId{};		//初始化为0	C++11
	std::mutex									m_idMutex;			////多线程之间保护m_baseUserId
};