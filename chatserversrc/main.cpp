#include "../base/logging.h"
#include "../jsoncpp-0.5.0/json.h"
#include "IMServer.h"

#include <stdio.h>
#include <memory>
#include <iostream>
#include <signal.h>
#include "../net/EventLoop.h"
#include "../base/singleton.h"
#include "../mysql/MysqlManager.h"
#include "../net/EventLoopThreadPool.h"
#include "UserManager.h"

using namespace std;

EventLoop g_mainLoop;

void prog_exit(int signo)
{
	std::cout << "program recv signal [" << signo << "] to exit." << std::endl;

	g_mainLoop.quit();
}
int main(int argc, char* argv[])
{
	//设置信号处理
	signal(SIGCHLD, SIG_DFL);
	signal(SIGPIPE, SIG_IGN);
	signal(SIGINT, prog_exit);
	signal(SIGKILL, prog_exit);
	signal(SIGTERM, prog_exit);

	short port = 0;

	if (0 == port)
		port = 12345;

	Logger::setLogLevel(Logger::INFO);

	//初始化数据库配置
	const char* dbserver = "127.0.0.1";
	const char* dbuser = "root";
	const char* dbpassword = "123456";
	const char* dbname = "myim";

	
	//检查数据库是否存在，数据表是否存在
	if (!Singleton<CMysqlManager>::Instance().Init(dbserver, dbuser, dbpassword, dbname))
	{
		LOG_FATAL << "please check your database config..............";
	}
	//获取所有好友信息，好友关系信息
	if (!Singleton<UserManager>::Instance().Init(dbserver, dbuser, dbpassword, dbname))
	{
		LOG_FATAL << "please check your database config..............";
	}

	Singleton<EventLoopThreadPool>::Instance().Init(&g_mainLoop, 4);
	Singleton<EventLoopThreadPool>::Instance().start();

	Singleton<IMServer>::Instance().Init("0.0.0.0", 30000, &g_mainLoop);

	g_mainLoop.loop();

	return 0;
}