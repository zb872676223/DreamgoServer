#include "queryresult.h"

QueryResult::QueryResult(MYSQL_RES * result, uint64_t rowCount, uint32_t fieldCount)
	:m_FieldCount(fieldCount),
	m_RowCount(rowCount),
	m_result(result)
{
	m_pCurrentRow = new Field[m_FieldCount];
	assert(m_pCurrentRow);
	MYSQL_FIELD *fields = mysql_fetch_fields(m_result);

	for (uint32_t i = 0; i < m_FieldCount; i++)
	{
		//TODO: 这个地方要不要判断为NULL？
		if (fields[i].name != nullptr)
		{
			m_mapFieldNames[i] = fields[i].name;
			m_vtFieldNames.emplace_back(fields[i].name);
		}
		else
		{
			m_mapFieldNames[i] = "";
			m_vtFieldNames.emplace_back("");
		}
		m_pCurrentRow[i].SetType(ConvertNativeType(fields[i].type));
	}
}

QueryResult::~QueryResult()
{
	EndQuery();
}

bool QueryResult::NextRow()
{
	MYSQL_ROW row;

	if (!m_result)
		return false;

	row = mysql_fetch_row(m_result);
	if (!row)
	{
		EndQuery();
		return false;
	}

	unsigned long int *ulFieldLength;
	ulFieldLength = mysql_fetch_lengths(m_result);
	for (uint32_t i = 0; i < m_FieldCount; i++)
	{
		if (row[i] == NULL)
		{
			m_pCurrentRow[i].m_bNULL = true;
			m_pCurrentRow[i].SetValue("", 0);
		}
		else
		{
			m_pCurrentRow[i].m_bNULL = true;
			m_pCurrentRow[i].SetValue(row[i], ulFieldLength[i]);
		}
		m_pCurrentRow[i].SetName(m_mapFieldNames[i]);
	}
	return true;
}

Field::DataTypes QueryResult::ConvertNativeType(enum_field_types mysqlType) const
{
	switch (mysqlType)
	{
	case FIELD_TYPE_TIMESTAMP:
	case FIELD_TYPE_DATE:
	case FIELD_TYPE_TIME:
	case FIELD_TYPE_DATETIME:
	case FIELD_TYPE_YEAR:
	case FIELD_TYPE_STRING:
	case FIELD_TYPE_VAR_STRING:
	case FIELD_TYPE_BLOB:
	case FIELD_TYPE_SET:
	case FIELD_TYPE_NULL:
		return Field::DB_TYPE_STRING;
	case FIELD_TYPE_TINY:

	case FIELD_TYPE_SHORT:
	case FIELD_TYPE_LONG:
	case FIELD_TYPE_INT24:
	case FIELD_TYPE_LONGLONG:
	case FIELD_TYPE_ENUM:
		return Field::DB_TYPE_INTEGER;
	case FIELD_TYPE_DECIMAL:
	case FIELD_TYPE_FLOAT:
	case FIELD_TYPE_DOUBLE:
		return Field::DB_TYPE_FLOAT;
	default:
		return Field::DB_TYPE_UNKNOWN;
	}
}

void QueryResult::EndQuery()
{
	if (m_pCurrentRow)
	{
		delete[] m_pCurrentRow;
		m_pCurrentRow = nullptr;
	}

	if (m_result)
	{
		mysql_free_result(m_result);
		m_result = nullptr;
	}
}
